package no.theaht.items.weapon;

import no.theaht.attributes.Attributes;
import no.theaht.items.Item;
import no.theaht.items.weapon.type.WeaponType;

public class Weapon extends Item {

    int baseDamage;
    WeaponType weaponType;
    int extraDamage;

    /**
     * Constructor. Sends the parameters name and level to the parent class item
     * Set the weapon type base damage.
     * */
    public Weapon(String name, int level, WeaponType weaponType){
        super(level, name);
        this.weaponType = weaponType;
        this.baseDamage = weaponType.setDamage(level);
    }

    // toString for printing the weapon
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Item stats for: ").append(super.getName()).append("\n")
                .append("Weapon type: ").append(weaponType.toString()).append("\n")
                .append("Weapon level: ").append(super.getLevel()).append("\n")
                .append("Base damage: ").append(baseDamage);
        return stringBuilder.toString();
    }

    /**
     * Method that returns the damage when a hero attacks.
     * The damage is calculated from the base damage and the extra damage based
     * on the hero's attributes.
     * */
    public int getAttackDamage(Attributes effectiveAttributes){
        extraDamage = weaponType.calculateExtraDamage(effectiveAttributes);
        return (baseDamage + extraDamage);
    }


}
