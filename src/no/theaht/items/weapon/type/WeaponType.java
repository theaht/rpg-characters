package no.theaht.items.weapon.type;

import no.theaht.attributes.Attributes;

public interface WeaponType {

    int setDamage(int level);
    String toString();
    int calculateExtraDamage(Attributes effectiveAttributes);

}
