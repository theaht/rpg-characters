package no.theaht.items.weapon.type;

import no.theaht.attributes.Attributes;

public class RangedWeapon implements WeaponType {

    /**
     * Set the base damage of the weapon
     * @param level the level of the weapon
     * */
    public int setDamage(int level){
        return 5+(3*level);
    }

    public String toString(){
        return "Ranged";
    }

    /**
     * Calculate the extra damage based on the dexterity of the hero.
     * @param effectiveAttributes the attributes of the hero
     * */
    public int calculateExtraDamage(Attributes effectiveAttributes){
        return 2* effectiveAttributes.getDexterity();
    }
}
