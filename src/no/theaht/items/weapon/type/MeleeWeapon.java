package no.theaht.items.weapon.type;

import no.theaht.attributes.Attributes;

public class MeleeWeapon implements WeaponType {

    /**
     * Set the base damage of the weapon
     * @param level the level of the weapon
     * */
    public int setDamage(int level){
        return 15+(level*2);
    }

    public String toString(){
        return "Melee";
    }

    /**
     * Calculate the extra damage based on the strength of the hero.
     * @param effectiveAttributes the attributes of the hero
     * */
    public int calculateExtraDamage(Attributes effectiveAttributes){
        return (int) (1.5*effectiveAttributes.getStrength());
    }

}
