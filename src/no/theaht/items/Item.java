package no.theaht.items;

public class Item {

    int level;
    String name;

    /**
     * Constructor to set the level and name of the item.
     * */
    public Item(int level, String name){
        this.level = level;
        this.name = name;

    }

    //Getters
    public int getLevel(){
        return this.level;
    }

    public String getName(){
        return this.name;
    }
}
