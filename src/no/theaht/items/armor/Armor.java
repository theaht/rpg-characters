package no.theaht.items.armor;

import no.theaht.attributes.Attributes;
import no.theaht.items.Item;
import no.theaht.items.armor.strategy.ArmorStrategy;
import no.theaht.items.armor.type.ArmorType;

public class Armor extends Item {
    ArmorStrategy armorStrategy;
    ArmorType armorType;
    Attributes bonuses;

    /**
     * Constructor.
     * Sends the parameters name and level to the parent class item.
     * calls the method set bonuses to set the attributes based on the armor strategy.
     * */
    public Armor(String name, int level, ArmorStrategy armorStrategy, ArmorType armorType){
        super(level, name);
        this.armorStrategy = armorStrategy;
        this.armorType = armorType;
        this.bonuses = new Attributes();
        setBonuses();
    }

    //toString for printing the armor
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Item stats for: ").append(super.getName()).append("\n")
                .append("Armor strategy: ").append(armorStrategy.toString()).append("\n")
                .append("Armor type: ").append(armorType.toString()).append("\n")
                .append("Armor level: ").append(super.getLevel()).append("\n")
                .append(bonuses.toString());
        return stringBuilder.toString();
    }

    /**
     * method calls the methods initiateBonuses and levelBonus
     * to set the attributes based on armor strategy.
     * Scale the attributes based on the armor type.
     * */
    public void setBonuses(){
       armorStrategy.initiateBonuses(this.bonuses);
       armorStrategy.levelBonus(this.bonuses, super.getLevel());
       armorType.scale(this.bonuses);
    }

    //Getters
    public Attributes getBonuses(){
        return this.bonuses;
    }

    public ArmorType getArmorType(){
        return armorType;
    }




}
