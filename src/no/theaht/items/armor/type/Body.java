package no.theaht.items.armor.type;

import no.theaht.attributes.Attributes;

public class Body implements ArmorType{

    /**
     * Scales the attributes based on what type of armor it is.
     * Body is scaled 100 % therefore the attributes are set to the same
     * @param bonuses the attributes of the armor
     * */
    public void scale(Attributes bonuses){
        bonuses.setHealth(bonuses.getHealth());
        bonuses.setStrength(bonuses.getStrength());
        bonuses.setIntelligence(bonuses.getIntelligence());
        bonuses.setDexterity(bonuses.getDexterity());
    }

    public String toString(){
        return "Body";
    }
}
