package no.theaht.items.armor.type;

import no.theaht.attributes.Attributes;

public class Legs implements ArmorType {

    /**
     * Scales the attributes based on what type of armor it is.
     * Legs are scaled 60 % therefore the attributes are multiplied with 0.6
     * casted to int to return a integer
     * @param bonuses the attributes of the armor
     * */
    public void scale(Attributes bonuses){
        bonuses.setHealth((int)(bonuses.getHealth()*0.6));
        bonuses.setStrength((int)(bonuses.getStrength()*0.6));
        bonuses.setIntelligence((int)(bonuses.getIntelligence()*0.6));
        bonuses.setDexterity((int)(bonuses.getDexterity()*0.6));
    }

    public String toString(){
        return "Legs";
    }

}
