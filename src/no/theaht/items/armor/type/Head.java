package no.theaht.items.armor.type;

import no.theaht.attributes.Attributes;

public class Head implements ArmorType{

    /**
     * Scales the attributes based on what type of armor it is.
     * Head is scaled 80 % therefore the attributes are multiplied with 0.8
     * casted to int to return a integer
     * @param bonuses the attributes of the armor
     * */
    public void scale(Attributes bonuses){
        bonuses.setHealth((int)(bonuses.getHealth()*0.8));
        bonuses.setStrength((int)(bonuses.getStrength()*0.8));
        bonuses.setIntelligence((int)(bonuses.getIntelligence()*0.8));
        bonuses.setDexterity((int)(bonuses.getDexterity()*0.8));
    }

    public String toString(){
        return "Head";
    }
}
