package no.theaht.items.armor.type;

import no.theaht.attributes.Attributes;

public interface ArmorType {
    void scale(Attributes bonuses);
    String toString();
}
