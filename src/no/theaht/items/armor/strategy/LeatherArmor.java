package no.theaht.items.armor.strategy;


import no.theaht.attributes.Attributes;
import no.theaht.items.armor.strategy.ArmorStrategy;

public class LeatherArmor implements ArmorStrategy {

    public String toString(){
        return "Leather";
    }

    /**
     * Set the base attributes for the armor
     * @param bonuses the attributes for the armor
     * */
    public void initiateBonuses(Attributes bonuses){
        bonuses.setHealth(20);
        bonuses.setDexterity(3);
        bonuses.setStrength(1);
    }

    /**
     * Increases the attributes for the armor, based at the armor level.
     * @param bonuses the attributes for the armor
     * @param level the level of the armor
     * */
    public void levelBonus(Attributes bonuses, int level){
        bonuses.setHealth(bonuses.getHealth()+(8*level));
        bonuses.setDexterity(bonuses.getDexterity()+(2*level));
        bonuses.setStrength(bonuses.getStrength()+(1*level));
    }

}
