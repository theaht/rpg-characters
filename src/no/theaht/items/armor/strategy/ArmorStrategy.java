package no.theaht.items.armor.strategy;

import no.theaht.attributes.Attributes;

public interface ArmorStrategy {

    void initiateBonuses(Attributes bonuses);
    void levelBonus(Attributes bonuses, int level);
    String toString();
}
