package no.theaht.items.armor.strategy;


import no.theaht.attributes.Attributes;
import no.theaht.items.armor.strategy.ArmorStrategy;

public class ClothArmor implements ArmorStrategy {

    public String toString(){
        return "Cloth";
    }

    /**
     * Set the base attributes for the armor
     * @param bonuses the attributes for the armor
     * */
    public void initiateBonuses(Attributes bonuses){
        bonuses.setHealth(10);
        bonuses.setIntelligence(3);
        bonuses.setDexterity(1);
    }

    /**
     * Increases the attributes for the armor, based at the armor level.
     * @param bonuses the attributes for the armor
     * @param level the level of the armor
     * */
    public void levelBonus(Attributes bonuses, int level){
        bonuses.setHealth(bonuses.getHealth()+(5*level));
        bonuses.setIntelligence(bonuses.getIntelligence()+(2*level));
        bonuses.setDexterity(bonuses.getDexterity()+(1*level));
    }
}
