package no.theaht;

import no.theaht.heroes.Hero;
import no.theaht.heroes.strategy.Mage;
import no.theaht.heroes.strategy.Ranger;
import no.theaht.heroes.strategy.Warrior;
import no.theaht.items.armor.Armor;
import no.theaht.items.armor.strategy.ClothArmor;
import no.theaht.items.armor.strategy.LeatherArmor;
import no.theaht.items.armor.strategy.PlateArmor;
import no.theaht.items.armor.type.Body;
import no.theaht.items.armor.type.Head;
import no.theaht.items.armor.type.Legs;
import no.theaht.items.weapon.type.MagicWeapon;
import no.theaht.items.weapon.type.MeleeWeapon;
import no.theaht.items.weapon.Weapon;
import no.theaht.items.weapon.type.RangedWeapon;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        main.demonstrateMage();
        //main.demonstrateRanger();
        //main.demonstrateWarrior();


    }

    /**
     * Methods generating heroes and items
     * Demonstrating the change in attributes when heroes are equipped with armor and weapon
     * and to see the attack damage when a hero attacks,
     * */

    public void demonstrateMage(){
        System.out.println("--------------------------------------------");
        System.out.println("Generating a mage");
        System.out.println("--------------------------------------------\n");
        Hero mage = new Hero(new Mage());
        System.out.println(mage.toString());
        System.out.println();

        System.out.println("Add 300 in XP, mage should move to level 3");
        System.out.println("---------------------------------------------\n");
        mage.addExperience(300);
        System.out.println(mage.toString());
        System.out.println();

        System.out.println("Add 75 in XP, mage should move to level 4");
        System.out.println("---------------------------------------------\n");
        mage.addExperience(75);
        System.out.println(mage.toString());
        System.out.println();

        System.out.println("Create items and equip the mage");
        System.out.println("---------------------------------------------\n");

        Weapon magicWeapon = new Weapon("A magic weapon", 3, new MagicWeapon());
        Armor boots = new Armor("Boots", 4, new LeatherArmor(), new Legs());
        Armor uniform = new Armor("Uniform", 3, new ClothArmor(), new Body());
        Armor helmet = new Armor("Helmet", 2, new PlateArmor(), new Head());

        System.out.println(magicWeapon.toString());
        System.out.println();
        System.out.println(boots.toString());
        System.out.println();
        System.out.println(uniform.toString());
        System.out.println();
        System.out.println(helmet.toString());
        System.out.println();
        System.out.println(mage.toString());
        System.out.println();

        System.out.println("-- Equipped mage with magicWeapon");
        mage.equip(magicWeapon);

        System.out.println(mage.toString());
        System.out.println();

        System.out.println("-- Equipped mage with boots");
        mage.equip(boots);

        System.out.println(mage.toString());
        System.out.println();

        System.out.println("-- Equipped mage with uniform");
        mage.equip(uniform);

        System.out.println(mage.toString());
        System.out.println();

        System.out.println("-- Equipped mage with helmet");
        mage.equip(helmet);

        System.out.println(mage.toString());
        System.out.println();

        System.out.println("Mage attacking");
        System.out.println("---------------------------------------------\n");

        System.out.println(mage.attack());
        System.out.println();

        System.out.println("Mage change weapon");
        System.out.println("---------------------------------------------\n");

        Weapon meleeWeapon = new Weapon("Melee weapon", 2, new MeleeWeapon());
        System.out.println(meleeWeapon.toString());
        System.out.println();

        mage.equip(meleeWeapon);
        System.out.println(mage.toString());
        System.out.println();


        System.out.println("Mage attacking");
        System.out.println("---------------------------------------------\n");
        System.out.println(mage.attack());

    }

    public void demonstrateRanger(){

        System.out.println("--------------------------------------------");
        System.out.println("Generating a ranger");
        System.out.println("--------------------------------------------\n");
        Hero ranger = new Hero(new Ranger());
        System.out.println(ranger.toString());
        System.out.println();

        System.out.println("Adding 600 xp to level up");
        System.out.println("----------------------------------------");
        ranger.addExperience(600);
        System.out.println(ranger.toString());
        System.out.println();

        System.out.println("Generating armor and equip the ranger");
        System.out.println("---------------------------------------------");
        Armor hat = new Armor("Hat", 5, new ClothArmor(), new Head());
        Armor body = new Armor("body armor", 4, new LeatherArmor(), new Body());
        Armor shoes = new Armor("shoes", 5, new PlateArmor(), new Legs());

        System.out.println(hat.toString());
        System.out.println();
        System.out.println(body.toString());
        System.out.println();
        System.out.println(shoes.toString());
        System.out.println();

        ranger.equip(hat);
        ranger.equip(body);
        ranger.equip(shoes);

        System.out.println(ranger.toString());
        System.out.println();

        System.out.println("Generating new armor and change the armor of the ranger");
        System.out.println("----------------------------------------------------------");
        Armor newHat = new Armor("New Hat", 1, new LeatherArmor(), new Head());
        Armor newBody = new Armor("new body armor", 2, new PlateArmor(), new Body());
        Armor newShoes = new Armor("new shoes", 2, new ClothArmor(), new Legs());

        System.out.println(newHat.toString());
        System.out.println();
        System.out.println(newBody.toString());
        System.out.println();
        System.out.println(newShoes.toString());
        System.out.println();

        ranger.equip(newHat);
        ranger.equip(newBody);
        ranger.equip(newShoes);

        System.out.println(ranger.toString());
        System.out.println();

    }

    public void demonstrateWarrior(){

        System.out.println("--------------------------------------------");
        System.out.println("Generating a warrior");
        System.out.println("--------------------------------------------\n");
        Hero warrior = new Hero(new Warrior());

        System.out.println(warrior.toString());
        System.out.println();

        System.out.println("Attacking with no weapon");
        System.out.println("--------------------------------------------\n");
        System.out.println(warrior.attack());
        System.out.println();

        System.out.println("Equipping the warrior with weapon and attacking");
        System.out.println("---------------------------------------------------");

        Weapon rangedWeapon = new Weapon("rangedWeapon", 1, new RangedWeapon());

        System.out.println(rangedWeapon.toString());
        System.out.println();

        warrior.equip(rangedWeapon);
        System.out.println(warrior.attack());
    }



}
