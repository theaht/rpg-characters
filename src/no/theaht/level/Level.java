package no.theaht.level;

public class Level {

    int currentLevel;
    int experiencePoints;
    int requiredPoints;

    public Level(){
        this.currentLevel = 1;
        this.experiencePoints = 0;
        this.requiredPoints = 100;
    }

    public boolean canLevelUp(){
        if(requiredPoints<= experiencePoints){
            return true;
        }
        return false;
    }

    public void levelUp(){
        this.currentLevel += 1;
        this.experiencePoints -= this.requiredPoints;
        calculateRequiredPoints();

    }

    public void calculateRequiredPoints(){
        double newRequired = this.requiredPoints*1.1;
        this.requiredPoints = (int) newRequired;
    }

    public void addExperiencePoints(int newPoints){
        experiencePoints+=newPoints;
    }

    public int getCurrentLevel(){
        return  currentLevel;
    }

    public int getXpToNext(){
        return requiredPoints-experiencePoints;
    }


}
