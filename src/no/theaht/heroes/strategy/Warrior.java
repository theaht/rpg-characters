package no.theaht.heroes.strategy;

import no.theaht.attributes.Attributes;

public class Warrior implements HeroStrategy {

    public String toString(){
        return "Warrior";
    }

    /**
     * Sets the base attributes
     * @param attributes the attributes where we set the values
     * */
    public void initiateAttributes(Attributes attributes) {
        attributes.setHealth(150);
        attributes.setStrength(10);
        attributes.setDexterity(3);
        attributes.setIntelligence(1);
    }

    /**
     * Increases the attributes when the warrior levels up.
     * @param attributes the attributes to increase
     * */
    public void levelUp(Attributes attributes){
        attributes.setHealth(attributes.getHealth()+30);
        attributes.setStrength(attributes.getStrength()+5);
        attributes.setDexterity(attributes.getDexterity()+2);
        attributes.setIntelligence(attributes.getIntelligence()+1);

    }

}
