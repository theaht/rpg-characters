package no.theaht.heroes.strategy;

import no.theaht.attributes.Attributes;

public class Mage implements HeroStrategy {

    public String toString(){
        return "Mage";
    }

    /**
     * Sets the base attributes
     * @param attributes the attributes where we set the values
     * */
    public void initiateAttributes(Attributes attributes) {
        attributes.setHealth(100);
        attributes.setStrength(2);
        attributes.setDexterity(3);
        attributes.setIntelligence(5);
    }

    /**
     * Increases the attributes when the mage levels up.
     * @param attributes the attributes to increase
     * */
    public void levelUp(Attributes attributes){
        attributes.setHealth(attributes.getHealth()+15);
        attributes.setStrength(attributes.getStrength()+1);
        attributes.setDexterity(attributes.getDexterity()+2);
        attributes.setIntelligence(attributes.getIntelligence()+5);

    }

}
