package no.theaht.heroes.strategy;

import no.theaht.attributes.Attributes;


public interface HeroStrategy {

    void levelUp(Attributes attributes);
    void initiateAttributes(Attributes attributes);
    String toString();

}
