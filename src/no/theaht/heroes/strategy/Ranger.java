package no.theaht.heroes.strategy;

import no.theaht.attributes.Attributes;


public class Ranger implements HeroStrategy {

    public String toString(){
        return "Ranger";
    }

    /**
     * Sets the base attributes
     * @param attributes the attributes where we set the values
     * */
    public void initiateAttributes(Attributes attributes) {
        attributes.setHealth(120);
        attributes.setStrength(5);
        attributes.setDexterity(10);
        attributes.setIntelligence(2);
    }

    /**
     * Increases the attributes when the ranger levels up.
     * @param attributes the attributes to increase
     * */
    public void levelUp(Attributes attributes){
        attributes.setHealth(attributes.getHealth()+20);
        attributes.setStrength(attributes.getStrength()+2);
        attributes.setDexterity(attributes.getDexterity()+5);
        attributes.setIntelligence(attributes.getIntelligence()+1);

    }



}
