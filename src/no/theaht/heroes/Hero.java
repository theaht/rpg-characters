package no.theaht.heroes;

import no.theaht.attributes.Attributes;
import no.theaht.heroes.strategy.HeroStrategy;
import no.theaht.items.Item;
import no.theaht.items.armor.Armor;
import no.theaht.items.armor.type.Body;
import no.theaht.items.armor.type.Head;
import no.theaht.items.armor.type.Legs;
import no.theaht.items.weapon.Weapon;
import no.theaht.level.Level;


public class Hero {

    HeroStrategy heroStrategy;
    Attributes attributes;
    Attributes effectiveAttributes;
    Level level;
    Armor head;
    Armor body;
    Armor legs;
    Weapon weapon;

    /**
     * Constructor.
     * Calls the methods initiateAttributes to set the base attributes
     * based on what type of hero strategy.
     * Calls the method setEffectiveAttributes to set them to have the same values as the base attributes.
     * @param  heroStrategy the type of hero
     * */
    public Hero(HeroStrategy heroStrategy){
        this.heroStrategy = heroStrategy;
        this.attributes = new Attributes();
        heroStrategy.initiateAttributes(attributes);
        this.level = new Level();
        this.effectiveAttributes = new Attributes();
        setEffectiveAttributes();

    }

    //toString to print the hero
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(heroStrategy.toString()).append(" details:").append("\n")
                .append(effectiveAttributes.toString()).append("\n")
                .append("Level: ").append(level.getCurrentLevel()).append("\n")
                .append("XP to next: ").append(level.getXpToNext());
        return stringBuilder.toString();
    }

    /**
     * Method to get the attack damage. If the hero is not equipped with a weapon,
     * the damage will be 0.
     * */
    public String attack(){
        if(weapon == null){
            return "Attacking for: " + 0;
        }
        return "Attacking for: " + weapon.getAttackDamage(effectiveAttributes);
    }

    /**
     * When a hero levels up, the method levelUp in the heroStrategy is called, to increase the attributes.
     * The effective attributes are updated with the increased base attributes.
     * the method levelUp in the class Level is called to set the new level and update xp.
     * */
    public void levelUp(){
        heroStrategy.levelUp(attributes);
        setEffectiveAttributes();
        level.levelUp();

    }

    /**
     * Called when the hero gains experience.
     * Updates the xp in level and check if the hero has enough xp to level up.
     * It will level up as many times possible
     * @param experience the amount of xp to add
     * */
    public void addExperience(int experience){
        level.addExperiencePoints(experience);
        while(level.canLevelUp()){
            this.levelUp();
        }
    }

    /**
     * Method to equip the hero with weapon or armor.
     * Checks if the hero is at a high enough level to equip the given item.
     * Based on the type of the item, it is added to the correct slot.
     * Updates the effective attributes when the item is added.
     * */
    public void equip(Item item){
        if(item.getLevel() <= level.getCurrentLevel()){
            if(item instanceof Weapon){
                this.weapon = (Weapon) item;
                setEffectiveAttributes();
            }
            else if(item instanceof Armor){
                if(((Armor) item).getArmorType() instanceof Head){
                        this.head = (Armor) item;
                    setEffectiveAttributes();
                }
                else if(((Armor) item).getArmorType() instanceof Body){
                    this.body = (Armor) item;
                    setEffectiveAttributes();
                }
                else if(((Armor) item).getArmorType() instanceof Legs){
                    this.legs = (Armor) item;
                    setEffectiveAttributes();
                }
            }
        }

    }

    /**
     * sets the effective attributes.
     * This is the base attributes from heroStrategy and level bonus +
     * extra attributes from each armor the the hero is equipped with.
     * */
    public void setEffectiveAttributes(){
        int health = attributes.getHealth();
        int strength = attributes.getStrength();
        int intelligence = attributes.getIntelligence();
        int dexterity = attributes.getDexterity();
        if(this.head != null){
            Attributes headAttributes = head.getBonuses();
            health += headAttributes.getHealth();
            strength += headAttributes.getStrength();
            intelligence += headAttributes.getIntelligence();
            dexterity += headAttributes.getDexterity();
        }
        if(this.body != null){
            Attributes bodyAttributes = body.getBonuses();
            health += bodyAttributes.getHealth();
            strength += bodyAttributes.getStrength();
            intelligence += bodyAttributes.getIntelligence();
            dexterity += bodyAttributes.getDexterity();
        }
        if(this.legs != null){
            Attributes legsAttributes = legs.getBonuses();
            health += legsAttributes.getHealth();
            strength += legsAttributes.getStrength();
            intelligence += legsAttributes.getIntelligence();
            dexterity += legsAttributes.getDexterity();
        }
        effectiveAttributes.setHealth(health);
        effectiveAttributes.setStrength(strength);
        effectiveAttributes.setDexterity(dexterity);
        effectiveAttributes.setIntelligence(intelligence);
    }




}
