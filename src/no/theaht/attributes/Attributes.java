package no.theaht.attributes;

public class Attributes{

    int health;
    int strength;
    int intelligence;
    int dexterity;

    /*
    * toString to print the attributes
    * */
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Health: ").append(health).append("\n")
                .append("Strength: ").append(strength).append("\n")
                .append("Dexterity: ").append(dexterity).append("\n")
                .append("Intelligence: ").append(intelligence);
        return stringBuilder.toString();
    }

    /*
    * setters and getters for each attribute
    * */

    public void setHealth(int health){
        this.health = health;
    }

    public void setStrength(int strength){
        this.strength = strength;
    }

    public void setIntelligence(int intelligence){
        this.intelligence = intelligence;
    }

    public void  setDexterity(int dexterity){
        this.dexterity = dexterity;
    }

    public int getHealth(){
        return this.health;
    }

    public int getStrength(){
        return this.strength;
    }

    public int getIntelligence() {
        return this.intelligence;
    }

    public int getDexterity(){
        return this.dexterity;
    }
}
